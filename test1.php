<?php
/**
 * Нужно написать код, который из массива выведет то что приведено ниже в комментарии.
 */
$x = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h'];

/*
print_r($x) - должен выводить это:
Array
(
    [h] => Array
        (
            [g] => Array
                (
                    [f] => Array
                        (
                            [e] => Array
                                (
                                    [d] => Array
                                        (
                                            [c] => Array
                                                (
                                                    [b] => Array
                                                        (
                                                            [a] =>
                                                        )

                                                )

                                        )

                                )

                        )

                )

        )

);*/

function make_array_keys( &$data, $value = null )
{
    $_ = array();

    foreach($data as $item) {
        $key = array_shift($data);

        $_[ $key ] = (count($data) > 0) ? make_array_keys($data, $value) : $value;
    }
    
    return $_;
}

print_r( make_array_keys($x) );