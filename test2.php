<?php
/**
 * Написать функцию которая из этого массива
 */
$data1 = [
    'parent.child.field' => 1,
    'parent.child.field2' => 2,
    'parent2.child.name' => 'test',
    'parent2.child2.name' => 'test',
    'parent2.child2.position' => 10,
    'parent3.child3.position' => 10,
];

//сделает такой
/*
$data = [
    'parent' => [
        'child' => [
            'field' => 1,
            'field2' => 2,
        ]
    ],
    'parent2' => [
        'child' => [
            'name' => 'test'
        ],
        'child2' => [
            'name' => 'test',
            'position' => 10
        ]
    ],
    'parent3' => [
        'child3' => [
            'position' => 10
        ]
    ],
];
*/

function make_array_keys( &$data, $value = null )
{
    $_ = array();

    foreach($data as $item) {
        $key = array_shift($data);

        $_[ $key ] = (count($data) > 0) ? make_array_keys($data, $value) : $value;
    }
    
    return $_;
}

function make_array_structutre( array $data )
{
    $_ = [];

    foreach($data as $key => $value) {
        $items = explode('.', $key);
        $_new  = make_array_keys($items, $value);
        $_     = array_merge_recursive($_, $_new);
    }

    return $_;
}

print_r( make_array_structutre($data1) );